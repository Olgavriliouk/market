var gulp = require('gulp'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  browserSync = require('browser-sync').create(),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  pump = require('pump'),
  cssnano = require('gulp-cssnano'),
  rename = require('gulp-rename'),
  del = require('del'),
  imagemin = require('gulp-imagemin'),
  cache = require('gulp-cache'),
  autoprefixer = require('gulp-autoprefixer'),
  gcmq = require('gulp-group-css-media-queries');

gulp.task('sass', function() {
  gulp
    .src('app/sass/styles.scss') //+(scss|sass)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({ browsers: ['last 2 versions'] }))
    // .pipe(cssnano())
    // .pipe(gcmq())
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('scripts', function(cb) {
  pump(
    [
      gulp.src([
        'app/libs/slick.min.js',
        'app/js/scripts.js'
      ]),
      sourcemaps.init(),
      concat('scripts.min.js'),
      uglify(),
      sourcemaps.write('../maps'),
      gulp.dest('app/js')
    ],
    cb
  );
});

gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      baseDir: 'app',
      url: 'http://192.168.1.250:3000'
    },
    notify: false
  });
});

gulp.task('clean', function() {
  return del.sync('dist');
});

gulp.task('clear', function() {
  return cache.clearAll();
});

gulp.task('img', function() {
  return gulp
    .src('app/img/**/*')
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.jpegtran({ progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo({
          plugins: [{ removeViewBox: true }, { cleanupIDs: false }]
        })
      ])
    )
    .pipe(gulp.dest('dist/img'));
});

gulp.task('watch', ['browser-sync', 'scripts', 'sass'], function() {
  gulp.watch('app/sass/**/*.scss', ['sass']);
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch('app/js/scripts.js', ['scripts'], browserSync.reload);
});

gulp.task('build', ['clean', 'scripts', 'img', 'sass'], function() {
  gulp.src('app/css/styles.css').pipe(gulp.dest('dist/css'));

  gulp.src('app/js/scripts.min.js').pipe(gulp.dest('dist/js'));

  gulp.src('app/fonts/**/*').pipe(gulp.dest('dist/fonts'));

  gulp.src('app/*.html').pipe(gulp.dest('dist/'));

  gulp.src('app/maps/*.map').pipe(gulp.dest('dist/maps'));
});

gulp.task('default', ['watch']);
